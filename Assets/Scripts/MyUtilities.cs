using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;


public class MyUtilities
{    

    public static PeelSession[] LoadXMLData(string filename)
    {
		List<PeelSession> cards = new List<PeelSession>();

		string bandName = "";
		string imageName = "";
		int numberOfSessions = 0;
		int numberOfSongs = 0;
		string firstSessionDate = "";
		string lastSessionDate = "";

		TextAsset xmlData = Resources.Load(filename) as TextAsset;
        string xmlString = xmlData.text;

        PeelSession session = null;

        // Create an XmlReader
        using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
        {
            while (reader.Read())
            {

                if (reader.LocalName == "session" && reader.IsStartElement())
                {
                    session = null;
                }
                if (reader.IsStartElement())
                {
                    switch (reader.LocalName)
                    {
                        case "band":
                            bandName = reader.ReadElementContentAsString();
                            break;
                        case "image":
                            imageName = reader.ReadElementContentAsString();
                            break;
                        case "number":
                            numberOfSessions = reader.ReadElementContentAsInt();
                            break;
                        case "songs":
                            numberOfSongs = reader.ReadElementContentAsInt();
                            break;
                        case "first":
                            firstSessionDate = reader.ReadElementContentAsString();
                            break;
                        case "last":
                            lastSessionDate = reader.ReadElementContentAsString();
                            
                            break;
                    }
                }
				if (reader.LocalName == "session" && !reader.IsStartElement())
				{
					session = new PeelSession(bandName,
									imageName,
									numberOfSessions,
									numberOfSongs,
									firstSessionDate,
									lastSessionDate);
					cards.Add(session);
				}
			}
        }   

		return cards.ToArray();
	}
}
