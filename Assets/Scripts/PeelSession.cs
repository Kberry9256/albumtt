﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeelSession 
{
    private string bandName;
    private string imageName;
    private int numberOfSessions;
    private int numberOfSongs;
    private string firstSessionDate;
    private string lastSessionDate;

    public string BandName { get => bandName; }
    public string ImageName { get => imageName; }
    public int NumberOfSessions { get => numberOfSessions; }
    public int NumberOfSongs { get => numberOfSongs; }
    public string FirstSessionDate { get => firstSessionDate; }
    public string LastSessionDate { get => lastSessionDate; }

    public PeelSession(string bandName, string imageName, int numberOfSessions, int numberOfSongs, string firstSessionDate, string lastSessionDate)
    {
        this.bandName = bandName;
        this.imageName = imageName;
        this.numberOfSessions = numberOfSessions;
        this.numberOfSongs = numberOfSongs;
        this.firstSessionDate = firstSessionDate;
        this.lastSessionDate = lastSessionDate;
    }
}
